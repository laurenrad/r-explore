from setuptools import setup, find_packages

# TODO: add long_description, test with lower python versions,
# check library version requirements
setup(
    name='R-Explore',
    version='0.7.0',
    author='Lauren Croney',
    author_email='laurenc.ist.rad@gmail.com',
    description='Sample disk browser for Roland 12-bit samplers',
    url="https://bitbucket.org/laurenrad/r-explore",
    license='MIT',
    # Add long_description rst later
    packages=find_packages(),
    install_requires=['simpleaudio>=1.0.3','matplotlib>=2.2.2,<= 3.2.2',
                      'numpy>=1.15.0'],
    python_requires='>=3.6',
    package_data={'rexplore.images': ['*.png'],
                  'rexplore.doc': ['*.txt'],
                  'rexplore.defaults': ['*.ini']},
    entry_points={'console_scripts': ['r-explore = rexplore:main']}
)
