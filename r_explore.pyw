# R-Explore - Launcher Module
# Lauren Croney 2020

from rexplore.main_gui import RootWindow

rw = RootWindow() # Launch GUI root window
rw.mainloop()
