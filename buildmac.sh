#!/bin/sh

# Build with cx_freeze
python3 cxsetup.py bdist_mac | tee build.log
cd build/R-Explore.app/Contents/MacOS

# Ad-hoc resign Python interpreter
codesign -f -s - ./Python

# Remove extra openblas library - takes up 100MB and isn't necessary
rm -v libopenblas.0.dylib
