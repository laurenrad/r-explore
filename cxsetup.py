from setuptools import find_packages
import cx_Freeze as cx
import platform


include_files=[('rexplore/images','images'),
               ('rexplore/doc','doc'),
               ('rexplore/defaults','defaults'),
               'LICENSE']
base = None
target_name = 'R-Explore'

# Windows build stuff
if platform.system() == "Windows":
    base = "win32GUI"
    target_name = 'rexplore.exe'
shortcut_data = [
('DesktopShortcut', 'DesktopFolder', 'R-Explore', 'TARGETDIR',
 '[TARGETDIR]' + target_name, None,
 'Sample Disk Browser', None,
 None, None, None, 'TARGETDIR'),
('MenuShortcut', 'ProgramMenuFolder', 'R-Explore', 'TARGETDIR',
 'TARGETDIR]' + target_name, None,
 'Sample Disk Browser', None,
 None, None, None, 'TARGETDIR')]
    

cx.setup (
    name='R-Explore',
    version='0.7.0',
    author='Lauren Croney',
    author_email='laurenc.ist.rad@gmail.com',
    description='Sample disk browser for Roland 12-bit samplers',
    url='https://bitbucket.org/laurenrad/r-explore',
    packages=find_packages(),
    executables=[
        cx.Executable('r_explore.pyw',base=base,
                      targetName=target_name,
                      icon='rexplore.ico')],
    options={
        'build_exe': {
            'packages': ['simpleaudio','matplotlib','dbm'],
            'includes': [],
            'excludes': ['PyQt4', 'PyQt5', 'PySide', 'IPython',
                         'jupyter_cylient', 'jupyter_core',
                         'ipykernel', 'ipython_genutils'],
            'include_files': include_files
            },
        'bdist_mac': {
            'bundle_name': 'R-Explore',
            'iconfile': 'rexplore.icns',
            'custom_info_plist': 'Info.plist'
            },
        'bdist_msi': {
            'upgrade_code': '{38D8C662-F01D-4F4C-B014-FE7C0B86458F}',
            'data': {'Shortcut': shortcut_data}
            }
        }
    )
            
