R-Explore
================================================
A sample disk browser for Roland 12-bit samplers

Features
--------
    * Play back wave samples
    * Export single wave sample, or export all samples to a zip archive
    * Export a text report containing parameter information
    * Browse parameters (see 'Format Support' section)

What It Doesn't Do
------------------
    * Edit disk data
    * Read in disk images; a sample disk must first be imaged with another program such as SDISKW or dd.

Format Support
--------------
Currently, R-Explore fully supports S-550 and S-330 disks. S-50 support is included but not tested.

Installing
----------
There is a source distribution in the dist folder.
To use the source distribution, run the install script with:
    python3 setup.py install
Depending on your Python installation, you may need to type 'python' instead of 'python3'.

Copying
-------
This program is released under the terms of the MIT License and comes with no
warranty. For more information, see the LICENSE file.
