"""
Init module for managing path to images.
"""
from os import path
import sys

if getattr(sys, 'frozen', False):
    IMAGE_DIR = path.join(path.dirname(sys.executable), 'images')
else:
    IMAGE_DIR = path.dirname(__file__)

ICON_16 = path.join(IMAGE_DIR, 'icon16.png')
ICON_22 = path.join(IMAGE_DIR, 'icon22.png')
ICON_32 = path.join(IMAGE_DIR, 'icon32.png')
ICON_48 = path.join(IMAGE_DIR, 'icon48.png')
ICON_PLAY = path.join(IMAGE_DIR, 'iconplay.png')
ICON_INFO = path.join(IMAGE_DIR, 'iconinfo.png')
ICON_EXPORT = path.join(IMAGE_DIR, 'iconexport.png')
