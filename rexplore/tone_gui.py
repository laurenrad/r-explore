"""
R-Explore: tone_gui.py
Tone detail view window.
"""

import os
import tkinter as tk
from tkinter import ttk
from tkinter.filedialog import asksaveasfilename

import rexplore.widgets as w
import rexplore.app as app
import rexplore.formats as formats
import rexplore.plots as plots

### Field definitions for creating the various tabs
### Some working vars and parameters with special handling aren't stored here.

# list of keys to display on 'Main' tab in tone window
# SOURCE TONE and ORIG/SUB TONE have special manual processing.
params_main_common = ['NAME', 'SOURCE TONE', 'ORIG KEY NUMBER', 'LEVEL',
                      'FINE TUNE', 'PITCH FOLLOW', 'WAVE SEGMENT TOP',
                      'WAVE SEGMENT LENGTH', 'FREQUENCY', 'WAVE BANK']
params_main_50 = params_main_common # no unique parameters for this one
params_main_550 = params_main_common + ['OSC LFO DEPTH', 'TONE OUTPUT ASSIGN',
                                        'TVF SWITCH', 'AFTER TOUCH SWITCH',
                                        'TRANSPOSE', 'BENDER SWITCH']

# list of keys to display on 'Loop' tab in tone window
# All params common to all formats
params_loop = ['LOOP MODE', 'LOOP TUNE', 'START POINT', 'LOOP LENGTH',
               'LOOP POINT', 'END POINT']

# list of keys to display on 'LFO' tab in tone window
params_lfo_50 = ['LFO RATE', 'LFO DEPTH', 'LFO DELAY'] # "pitch mod"
params_lfo_550 = ['LFO RATE', 'LFO SYNC', 'LFO MODE', 'LFO DELAY',
                  'LFO OFFSET', 'LFO POLARITY']

# list of keys to display on 'TVA' tab in tone window
params_tva_550 = ['TVA LFO DEPTH', 'TVA LEVEL CURVE', 'ENV VEL-RATE',
                  'TVA ENV KEY-RATE']
params_tva_50 = ['LEVEL CURVE', 'ENV KEY-RATE', 'ENV VEL-RATE']

# List of keys to display on 'TVF' tab in tone window
# TVF params are specific to S-550.
params_tvf = ['TVF CUT OFF', 'TVF RESONANCE', 'TVF KEY FOLLOW',
              'TVF LFO DEPTH', 'TVF LEVEL CURVE', 'TVF EG DEPTH',
              'TVF EG POLARITY', 'TVF KEY RATE FOLLOW',
              'TVF VELOCITY RATE FOLLOW']

class ParamsTab(ttk.Frame):
    """Create a tab to display parameters specified in a list (new)"""
    def __init__(self,parent,param_list,*args,**kwargs):
        super().__init__(parent,*args,**kwargs)
        
        self.params_frame = w.ToneParamFrame(self)
        self.params_frame.grid_columnconfigure(0,weight=1,minsize=100)
            
        for field in param_list:
            self.params_frame.add_row(field) # add empty parameter views to the frame
        
        self.params_frame.pack(fill=tk.BOTH,expand=1, padx=5)
        
        # Envelope views
        if param_list is params_tvf:
            self.env_view = w.EnvelopeView(self,'TVF',text='Envelope')
            self.env_view.pack(fill=tk.X,padx=5,side=tk.BOTTOM)
        elif (param_list is params_tva_550 or
              param_list is params_tva_50):
            self.env_view = w.EnvelopeView(self,'TVA',text='Envelope')
            self.env_view.pack(fill=tk.X,padx=5,side=tk.BOTTOM)
        else:
            self.env_view = None
        
        
    def refresh(self,num):
        """Refresh this view for a new tone specified by 'num'."""
        self.params_frame.refresh(num)
        if self.env_view:
            self.env_view.refresh(num)
            
# Tab box for organizing and displaying tone parameters
class ToneTabBox(ttk.Notebook):
    """Displays information about a single tone in a tabbed view."""
    

    
    def __init__(self,parent,*args,**kwargs):
        super().__init__(parent,*args,**kwargs)

        # Create tabs for each category defined in ToneFields and add them
        self.main_tab_50 = ParamsTab(self,params_main_50)
        self.main_tab_550 = ParamsTab(self,params_main_550)
        self.loop_tab = ParamsTab(self,params_loop)
        self.lfo_tab_50 = ParamsTab(self,params_lfo_50)
        self.lfo_tab_550 = ParamsTab(self,params_lfo_550)
        self.tvf_tab = ParamsTab(self,params_tvf)
        self.tva_tab_550 = ParamsTab(self,params_tva_550)
        self.tva_tab_50 = ParamsTab(self,params_tva_50)
        
    def refresh(self,num):
        """Refresh this view for a new tone specified by 'num'."""
        
        # Format-specific tabs
        if isinstance(app.get_disk_format(),formats.S50):
            self.main_tab_50.refresh(num)
            self.lfo_tab_50.refresh(num)
            self.tva_tab_50.refresh(num)
        elif isinstance(app.get_disk_format(),formats.S550):
            self.main_tab_550.refresh(num)
            self.lfo_tab_550.refresh(num)
            self.tva_tab_550.refresh(num)
            self.tvf_tab.refresh(num)
        else:
            print("DEBUG: BAD FORMAT IN TONE GUI")
        
        self.loop_tab.refresh(num) # Common to both
        
        
    def update_layout(self):
        """
        Adjust layout for disk format.
        """
        for tab in self.tabs():
            self.forget(tab)
        if isinstance(app.get_disk_format(),formats.S50):
            self.add(self.main_tab_50,text='Main')
            self.add(self.loop_tab,text='Loop')
            self.add(self.lfo_tab_50,text='LFO')
            self.add(self.tva_tab_50,text='Envelope')
        elif isinstance(app.get_disk_format(),formats.S550):
            self.add(self.main_tab_550,text='Main')
            self.add(self.loop_tab,text='Loop')
            self.add(self.lfo_tab_550,text='LFO')
            self.add(self.tvf_tab,text='TVF')
            self.add(self.tva_tab_550,text='TVA')
        else:
            print("DEBUG: BAD FORMAT IN TONE GUI")
        
class ToneView(ttk.Frame):
    def __init__(self,parent,*args,**kwargs):
        super().__init__(parent,*args,**kwargs)
        #self.pack_propagate(True)
        #self.grid_propagate(True)
        current_index = 0 # Current tone shown
        self.tab_box = ToneTabBox(self)
        self.tab_box.pack(fill=tk.BOTH,expand=1,pady=5)
        self.empty = True # flag tones with no wave data
        
        # Waveform view
        self.waveform = plots.WaveformView(self,height=50)
        self.waveform.pack(padx=5,pady=5,fill=tk.X)
        
        # play button
        self.play_button = ttk.Button(self,text="Play",command=self._on_play,state='disabled')
        self.play_button.pack(side=tk.RIGHT,padx=5,pady=5)

        # export button - disabled until a tone is selected
        self.export_button = ttk.Button(self,text="Export Wave",command=self._single_export,state='disabled')
        self.export_button.pack(side=tk.RIGHT,padx=5,pady=5)
        
    def refresh(self):
        """Update values and change layout; called on new file open."""
        self.view_item(0)
        self.tab_box.update_layout()
        
    def view_item(self,num):
        """Show values for a newly selected tone without changing layout."""
        self.current_index = num
        self.tab_box.refresh(num)
        
        self.export_button.config(state='normal')
        if app.get_disk().tones[self.current_index].length() != 0:
            self.empty = False
            self.play_button.config(state='normal') # enable play if not empty
            #self.waveform.clear()
            self.waveform.show_tone(self.current_index) # reload waveform view
        else:
            print("DEBUG: Empty tone")
            self.empty = True
            self.play_button.config(state='disabled') # otherwise disable
            self.waveform.set_empty() # clear waveform view
        
    def _on_play(self):
        if not self.empty:
            app.play(self.current_index)

    def _single_export(self):
        """Export wave data for a single tone."""
        default = app.get_disk().tones[self.current_index].get_name().strip() + '.wav'
        filename = asksaveasfilename(initialdir=app.get_initial_exportdir(),
                                     initialfile=default,
                                     defaultextension='.wav',
                                     filetypes = (("WAV (16-bit PCM)", "*.wav"),),
                                     title="Choose a file")
        
        # File dialog will be empty string if cancel button pressed
        # for some reason if empty tuple if window is closed
        if filename != '' and filename != ():
            app.export_tone(self.current_index,filename)
            app.save_prev_exportdir(filename) # Save previous dir for filechooser, if used
