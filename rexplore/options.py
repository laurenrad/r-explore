"""
R-Explore: options.py
Module for options dialog.
"""
import configparser
import os
import tkinter as tk
from tkinter import ttk
import rexplore.app as app
import rexplore.widgets as w

class DirOptFrame(ttk.Frame):
    def __init__(self,parent,label,*args,**kwargs):
        super().__init__(parent,*args,**kwargs)
        
        # Set grid weights
        self.grid_columnconfigure(0,weight=0)
        self.grid_columnconfigure(1,weight=1)
        self.grid_columnconfigure(2,weight=0)
        
        # Widget backing vars
        self.opt_var = tk.StringVar()
        self.customdir_var = tk.StringVar()
        
        # Create frame label
        lbl = ttk.Label(self,text=label,width=20,anchor='w')
        lbl.grid(row=0,column=0,sticky=tk.W)

        self.opt_combo = ttk.Combobox(self,textvariable=self.opt_var,
                                      state='readonly',width=60)
        self.opt_combo['values'] = ('Use home directory',
                                    'Remember last directory',
                                    'Use custom')
        self.opt_combo.bind('<<ComboboxSelected>>',self._on_combo)
        self.opt_combo.grid(row=0,column=1,columnspan=2,sticky='WE')
        
        self.customdir_label = ttk.Label(self,text='Custom:')
        self.customdir_label.grid(row=1,column=0,sticky=tk.W)
        self.entry = ttk.Entry(self,textvariable=self.customdir_var,
                              state='readonly')

        self.entry.grid(row=1,column=1,sticky='WE')
        self.browse_button = ttk.Button(self,text='Browse...',
                                       command=self._on_browse,
                                       state='disabled')
        self.browse_button.grid(row=1,column=2,sticky='E')
        
    def _on_browse(self):
        """Handler for custom directory entry field."""
        directory = tk.filedialog.askdirectory(mustexist=True,initialdir=app.home_path)
        self.customdir_var.set(directory)
        
    def _on_combo(self,event):
        """Event handler for directory combobox."""
        self.selection_clear() # Deselect text when making a selection
        if self.opt_combo.get() == 'Use custom':
            self.entry.config(state='readonly')
            self.browse_button.config(state='normal')
            self.customdir_label.config(state='normal')
        else:
            self.entry.config(state='disabled')
            self.browse_button.config(state='disabled')
            self.customdir_label.config(state='disabled')
            
    def get_combo(self):
        """Return the combobox selection."""
        return self.opt_combo.get()
    
    def get_entry(self):
        """Return the custom directory."""
        return self.entry.get()
    
    def set_combo(self,value):
        """Set the combobox selection."""
        self.opt_combo.set(value)
        self._on_combo(None) # Enable/disable other fields
        
    def set_entry(self,value):
        """Set the default directory entry."""
        self.customdir_var.set(value)

class OptionsWindow(tk.Toplevel):

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.title("R-Explore Options")
        self.resizable(width=False,height=False)
        
        # Field variables
        self.diskdir_opt = tk.StringVar()
        self.export_opt = tk.StringVar()
        self.disk_dir = tk.StringVar()
        self.use_loop_points = tk.BooleanVar()
        
        # Create view
        frame = ttk.Frame(self) # to apply ttk theme to background
        frame.grid_columnconfigure(0,weight=1) # grow widgets that span window
        self.diskdir = DirOptFrame(frame,'Default disk directory:')
        self.diskdir.grid(row=0,column=0,sticky='WE',columnspan=2)
        self.exportdir = DirOptFrame(frame,'Default export directory:')
        self.exportdir.grid(row=1,column=0,sticky='WE',columnspan=2)
        
        ttk.Checkbutton(frame,text='Use start and end points for play and export',
                        variable=self.use_loop_points).grid(row=2,column=0,
                                                            sticky=tk.W)
        
        cancelbutton = ttk.Button(frame,text="Cancel",command=self.destroy)
        cancelbutton.grid(row=3,column=0,sticky='E')
        savebutton = ttk.Button(frame,text="Save",command=self._on_save)
        savebutton.grid(row=3,column=1)
        frame.pack()
        
        # Populate view with config values
        self._load_config()
    
    def _load_config(self):
        """Read values from the config file and display them."""
        app.config.read(app.config_file)
        if 'FILE' in app.config:
            # Disk dir
            disk_opt = app.config['FILE'].get('diskdir_opt')
            disk_custom = app.config['FILE'].get('custom_diskdir',
                                                 app.home_path)
            self.diskdir.set_combo(disk_opt)
            self.diskdir.set_entry(disk_custom)
            # Export dir
            export_opt = app.config['FILE'].get('exportdir_opt')
            export_custom = app.config['FILE'].get('custom_exportdir',
                                                   app.home_path)
            self.exportdir.set_combo(export_opt)
            self.exportdir.set_entry(export_custom)
        if 'WAVE' in app.config:
            self.use_loop_points.set(app.config['WAVE'].getboolean('use_loop_points',
                                                                    fallback=False))

    def _on_save(self):
        app.config['FILE']['diskdir_opt'] = self.diskdir.get_combo()
        app.config['FILE']['custom_diskdir'] = self.diskdir.get_entry()
        app.config['FILE']['exportdir_opt'] = self.exportdir.get_combo()
        app.config['FILE']['custom_exportdir'] = self.exportdir.get_entry()
        app.config['WAVE']['use_loop_points'] = str(self.use_loop_points.get())
        
        with open(app.config_file, 'w') as configfile:
            app.config.write(configfile)
            
        self.destroy()

