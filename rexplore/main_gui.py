"""
R-Explore: main_gui.py
This module creates the main GUI window for R-Explore.
"""

import _thread as thread
import os

import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename



# package specific imports
import rexplore.app as app

import rexplore.formatting as formatting
import rexplore.widgets as w
import rexplore.menus as menus
import rexplore.options as options
import rexplore.formats
import rexplore.tab_gui as tabs
import rexplore.tone_gui as tone_gui
import rexplore.patch_gui as patch_gui
from rexplore.exceptions import *
from .images import * # window icon
from .doc import README # README file

class TabBox(ttk.Notebook):
    """Tabbed browsing view for disk data"""
    def __init__(self,parent,*args,**kwargs):
        super().__init__(parent,*args,**kwargs)
        
        self.function_tab = tabs.FunctionFrame()
        self.midi_tab = tabs.MidiFrame()
        self.tone_tab = tabs.ListBrowser(tone_gui.ToneView)
        self.patch_tab = tabs.ListBrowser(patch_gui.PatchView)
        self.add(self.function_tab,text='Function',state='disabled')
        self.add(self.midi_tab,text='MIDI',state='disabled')
        self.add(self.patch_tab,text='Patches',state='disabled')
        self.add(self.tone_tab,text='Tones',state='disabled')
        
    def enable(self):
        """Enable all tabs and select the first tab."""
        for tab in self.tabs():
            self.tab(tab,state='normal')
        self.select(0)
        
    def disable(self):
        """Disable all tabs."""
        for tab in self.tabs():
            self.tab(tab,state='disabled')
        
    def refresh(self):
        """
        Call refresh methods for tabs.
        """
        self.function_tab.refresh()
        self.midi_tab.refresh()
        self.patch_tab.refresh()
        self.tone_tab.refresh()
        
class AboutWindow(tk.Toplevel):
    """Custom About dialog."""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title("About R-Explore")
        self.resizable(width=False,height=False)
        self.description = tk.StringVar() # Main description
        self.description.set('Sample disk browser for Roland 12-bit samplers.')
        self.version = tk.StringVar() # StringVar of version
        self.version.set('Version ' + app.version)
        self.body = tk.StringVar() # Other body text
        self.body.set('Copyright 2020 Lauren Croney\n' + 'This program comes with NO WARRANTY ' +
                      'and is released under the terms of the MIT License. ' +
                      'See LICENSE file for more information, or ' +
                      'https://opensource.org/licenses/MIT \n')
        self.icon = tk.PhotoImage(file=ICON_48) # Program icon
        wrap = 250 # word wrap length for text

        frame = ttk.Frame(self) # To apply ttk styling to background
        ttk.Label(frame,image=self.icon).pack()
        ttk.Label(frame,textvariable=self.description,wraplength=wrap).pack()
        ttk.Label(frame,textvariable=self.version,justify=tk.LEFT).pack()
        ttk.Label(frame,textvariable=self.body,justify=tk.LEFT,wraplength=wrap).pack()
        ttk.Button(frame,text='Close',command=self.destroy).pack(side=tk.RIGHT)
        frame.pack()
            
class HelpViewer(tk.Toplevel):
    """Help viewer window."""
    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        self.title("R-Explore: Help Viewer")
        self.resizable(width=False,height=False)
        text = open(README).read() # just read in the readme
        frame = ttk.Frame(self) # To apply ttk styling to background
        
        # Text box - change the background so it's obvious you can't enter text.
        self.text_box = tk.Text(frame,wrap=tk.WORD,bg='#D9D9D9')
        self.text_box.insert('1.0',text)
        self.text_box.configure(state='disabled') # best solution to make read-only for now
        
        # Add scrollbar
        self.scrollbar = ttk.Scrollbar(frame,orient=tk.VERTICAL,command=self.text_box.yview)   
        
        self.scrollbar.grid(row=0,column=1,sticky='NSW')
        self.text_box.configure(yscrollcommand=self.scrollbar.set)
        self.text_box.grid(row=0,column=0)
        
        # Add ok button
        ttk.Button(frame,text="Ok",command=self.destroy).grid(row=1,columnspan=2,sticky=tk.E)
        self.scrollbar.config(command=self.text_box.yview)

        frame.pack()


class RootWindow(tk.Tk):
    """Main application interface"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title("R-Explore")
        style = ttk.Style()
        #style.theme_use('clam')
        #style.configure('Frame',background='ivory3')
        #style.configure('TLabelframe',background='ivory3')
        self.minsize(460,675) # set minimum size for window
        self.grid_columnconfigure(0,weight=1) # Grow horizontally
        self.grid_rowconfigure(0,weight=1) # Grow tab box vertically
        #self.resizable(width=False,height=False)
        self.disk = app.get_disk() # get reference to disk
        self.protocol("WM_DELETE_WINDOW", self._on_quit) # make sure cleanup happens on window close
        self.taskbar_icon = tk.PhotoImage(file=ICON_32)
        self.call('wm', 'iconphoto', self._w, self.taskbar_icon)
        
        # bindings for main menu
        self.menu_callbacks = {'file->open': self._on_open,
                               'file->exportall': self._on_export_all,
                               'file->exportreport': self._on_export_report,
                               'file->options': self._on_options,
                               'file->quit': self._on_quit,
                               'debug->test': self._on_test,
                               'debug->testfile': self._open_testfile,
                               'debug->testfile2': self._open_testfile2,
                               'help->about': self._on_about,
                               'help->view': self._on_help}
        # create main menu
        if self.tk.call('tk', 'windowingsystem') == 'aqua':
            self.menubar = menus.MainMenuMac(self,self.menu_callbacks)
        else:
            self.menubar = menus.MainMenu(self,self.menu_callbacks)
            
        if app.hacker_mode:
            self.menubar.enable_debug() # Enable debug entries
        self.config(menu=self.menubar)
    
        # create tabbed interface to show disk data
        self.tab_box = TabBox(self)
        self.tab_box.grid(row=0,column=0,sticky=tk.NSEW)
        
        # Status bar
        status_bar = ttk.Frame(self,borderwidth=2,relief="ridge")
        # create status label at the bottom for currently open disk file
        self.file_label = tk.StringVar()
        self.file_label.set('No file open.')
        file_display = ttk.Label(status_bar,textvariable=self.file_label,
                                anchor=tk.W)
        file_display.grid(row=0,column=0,sticky=tk.W)
        
        # os version label on status bar
        self.ver_string = tk.StringVar()
        self.ver_string.set('')
        ver_display = ttk.Label(status_bar,textvariable=self.ver_string,
                               anchor=tk.E)
        ver_display.grid(row=0,column=1,sticky=tk.E)
        status_bar.grid(row=1,column=0,sticky=tk.EW)
        status_bar.grid_columnconfigure(0,weight=1) # Set left column to grow
        status_bar.grid_columnconfigure(1,weight=1) # Set right column to grow

    
    def _on_export_all(self):
        """Open a file chooser and export all samples to a zip file."""
        filename = asksaveasfilename(initialdir=app.get_initial_exportdir(),
                                     defaultextension='.zip',
                                     filetypes = (("Compressed archive (ZIP)", "*.zip"),),
                                     title="Choose a file")
        
        print(f'DEBUG: filename is {filename}')
        # File dialog will be empty string if cancel button pressed
        # for some reason if empty tuple if window is closed
        if filename != '' and filename != ():
            pw = w.ProgressWindow()
            pw.grab_set() # grab focus
            pw.transient(self) # keep on top of other app windows
            # Export the samples, and pass along progress bar callback
            total = app.get_disk().get_wave_count()
            app.export_all(filename,callback=pw.update_progress,steps=32) # 32 tones, 32 steps
            app.save_prev_exportdir(filename) # Save previous dir for filechooser, if used
            
    def _on_export_report(self):
        """Callback for exporting a disk report."""
        filename = asksaveasfilename(initialdir=app.get_initial_exportdir(),
                                     defaultextension='.txt',
                                     filetypes=(("Text file", "*.txt"),),
                                     title="Choose a file")
        
        # File dialog will be empty string if cancel button pressed
        # for some reason if empty tuple if window is closed
        if filename != '' and filename != ():
            app.get_disk().export_disk_report(filename)
            app.save_prev_exportdir(filename) # Save previous dir for filechooser, if used


    def _on_quit(self):
        self.destroy()
        app.cleanup() # won't get called if app just dies, see if this can be caught
        print('Quitting...')

    def _on_about(self):
        """Display an 'about' dialog for the application."""
        aw = AboutWindow()
        aw.transient(self) # keep on top of other app windows
        
    def _on_help(self):
        hv = HelpViewer()
        hv.mainloop()
        
    def _on_options(self):
        options.OptionsWindow().transient(self)
        
    def _open_file(self, filename:str):
        """Open a disk image."""
        try:
            app.new_disk(filename)
        except FormatError as E:
            print(E)
            print('DEBUG: Unable to open new disk.')
            messagebox.showerror(title="File error",
                                 message="Invalid or unsupported disk format!",
                                 detail=str(E))
        except FileSizeError as E:
            print(E)
            messagebox.showerror(title="File error",
                                 message="File is too large to open!",
                                 detail=str(E))
        else:
            if not app.disk_is_empty():
                app.close_disk() # if there is already a disk open, clean up after it first
            self.file_label.set(f'Current file: {os.path.basename(filename)}') # update status display
            self.ver_string.set(app.get_disk().os_ver) # update status display: system version
            self.tab_box.refresh() # refresh tab box
            self.tab_box.enable() # ensure tabs are enabled
            
            # enable menu entries that are disabled on startup.
            # First argument to entryconfig is a positional index
            self.menubar.file_menu.entryconfig(1,state='normal') # Export All Samples
            self.menubar.file_menu.entryconfig(2,state='normal') # Export Disk Report
            
    def _open_testfile(self):
        """Open a test disk image (S-50)."""
        filename = os.path.join(app.home_path,'501_1.OUT')
        self._open_file(filename)
            
    def _open_testfile2(self):
        """Open a test disk image (S-550)."""
        filename = os.path.join(app.home_path,'DEV.OUT')
        self._open_file(filename)
            
    def _on_open(self):
        """Open a new disk image."""
        filename = askopenfilename(initialdir=app.get_initial_diskdir(),
                                       filetypes =(("Sample Disk Image", ("*.OUT","*.s55")),
                                                   ("All Files","*.*")),
                                       title="Choose a file")
        
        print(f'New filename: {filename}')
        # File dialog will be empty string if cancel button pressed
        # for some reason if empty tuple if window is closed
        if filename != '' and filename != ():
            app.save_prev_diskdir(filename) # Save previous dir for filechooser, if used
            self._open_file(filename)


    def _on_test(self):
        """For testing whatever needs to be tested at the moment."""
        filename = os.path.join(app.home_path,'DEV.OUT')
        app.new_disk(filename)
            

