"""
R-Explore: plots.py
This module contains plot and graph views for R-Explore.
"""

import gc
import ctypes
import matplotlib
import tkinter as tk
from tkinter import ttk
matplotlib.use('TkAgg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk )
import pylab

import rexplore.app as app

class WaveformView(tk.Frame):
    """
    A frame class to display a sample waveform.
    Uses matplotlib and pylab.
    Hopefully start and end points can be added in the future, but
    the values will need to be scaled to the converted audio.
    """
    def __init__(self,parent,*args,**kwargs):
        super().__init__(parent,*args,**kwargs)

        # Create Figure
        self.fig = Figure(figsize=(3.5,1.8),facecolor='#000000',tight_layout=True)
        self.plt = self.fig.add_subplot(111,xmargin=0,ymargin=0)
        
        # Create Canvas to hold figure
        self.canvas = FigureCanvasTkAgg(self.fig,self)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(fill=tk.BOTH,expand=True)
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        
        
    def show_tone(self, num):
        """Plot a tone's wave data."""
        gc.collect() # Somehow, without manual garbage collection this leaks quite a bit
        self.plt.clear() # Clear old plot data
        self.plt.axis('off') # Hide axis display - doesn't work if this isn't done
        # TBD: right now this just reads the entire buffer in at once
        sample = pylab.frombuffer(app.get_sample(num).tobytes(),ctypes.c_int16)
        self.plt.plot(sample,linewidth=0.5,color='#FFFFFF')
        # Plot start and end points
        start = app.get_tone_value('START POINT',num)
        loop = app.get_tone_value('LOOP POINT',num)
        end = app.get_tone_value('END POINT',num)
        self.plt.axvline(start,color='#FF0000',label='Start')
        self.plt.axvline(end,color='#00FF00',label='End',ls='--')
        self.plt.axvline(loop,color='#00BFFF',label='Loop',ls=':')
        self.plt.legend(loc='upper center',ncol=3,fancybox=False,
                        facecolor='white',bbox_to_anchor=(0.5, -0.05))
        
        

        
        self.canvas.draw()

        
    def set_empty(self):
        """Called to display an empty plot when the tone is empty."""
        self.plt.clear()
        self.plt.axis('off')
        self.canvas.draw()
        
