"""
R-Explore: app.py
This module contains controller data and functions for R-Explorer's frontend
to interact with the core disk handling modules. It also manages a global
disk instance.
"""
import simpleaudio as sa
import sys
import os
from os import environ
import platform
import tempfile
import configparser
import pathlib # for export path
import shelve # primarily for remembering current directory
from zipfile import ZipFile
from rexplore.diskread import Disk
from rexplore.exceptions import *
from .defaults import DEFAULTS
import _thread as thread


def get_disk():
    """Return the current disk instance."""
    return __disk

def get_patch_value(key:str, num:int):
    """
    Get a value from a given patch by key.
    Return None if no disk is open.
    """
    if disk_is_empty():
        return None
    else:
        return __disk.patches[num].lookup(key)
    
def get_tone_value(key:str, num:int):
    """
    Get a value from a given tone by key.
    Return None if no disk is open.
    """
    if disk_is_empty():
        return None
    else:
        return __disk.tones[num].lookup(key)
    
def get_function_value(key:str):
    """
    Get a value from the function block of the disk by key.
    Return None if no disk is open.
    """
    if disk_is_empty():
        return None
    else:
        return __disk.function.lookup(key)
    
def get_midi_value(key:str):
    """
    Get a value from the function block of the disk by key.
    Return None if no disk is open.
    """
    if disk_is_empty():
        return None
    else:
        return __disk.midi.lookup(key)

def get_disk_format():
    """Returns the format of the current disk."""
    return __disk.format

def get_disk_label() -> str:
    """Return disk label, or empty string if disk is empty."""
    label = __disk.function.lookup('DISK LABEL')
    if label is None:
        return ''
    else:
        return label

def get_tone_name(num: int):
    """Get a tone's name from its index."""
    return __disk.tones[num].get_name()

def get_sample(num:int):
    """
    Get the wave data from a tone and return it as a Bytes object.
    (This just calls the appropriate method from the Disk object.)
    """
    return __disk.convert_wave(num)

def new_disk(filename: str):
    """Open a new disk image."""
    global __disk
    try:
        __disk = Disk(filename)
    except Exception as E:
        print(f"DEBUG: new_disk caught exception: {E}")
        raise E # pass up to interface
    
def close_disk():
    """Clean up without removing temporary directory."""
    # clean up temporary wave files
    for i in range(0,32):
        fname = __tempdir.name + os.sep + f'{i}.wav'
        if os.path.exists(fname):
            print(f"DEBUG: removing {fname}")
            os.remove(fname)

def export_tone(num: int, filename:str):
    """Export a tone's wave data"""
    # for now, give up if empty. this checking is also done in extract_wave,
    # but this should be replaced later anyway [TMP]
    
    if __disk.tones[num].length() == 0:
        print("DEBUG: export_tone(): Empty tone!")
    else:
        use_loop = bool(get_bool_option('WAVE','use_loop_points',fallback=False)) # Get loop point option
        __disk.export_wav(num, filename, use_loop) # create a WAV file from the tone
        
def export_all(filename: str, callback=None, **cb_kwargs):
    """
    Export all samples from disk and compress to a zipfile.
    Callback is a callback function which updates on progress, and will
    receive any additional keyword args.
    """

    # For each possible tone, export a wave file and add it to the zip file
    with ZipFile(filename, 'w') as zipfile:
        for i in range(0,__disk.format.TONE_COUNT):
            if __disk.tones[i].length() != 0:
                path_temp = __tempdir.name + os.sep + f'{i}.wav' # path of temporary file
                path_out = 'samples' + os.sep + f'{i}.wav' # path of output file in archive
                export_tone(i,path_temp)
                # arcname is the name within the archive. this prevents
                # the archive from containing the temp directory
                zipfile.write(path_temp,arcname=path_out)
            if callback:
                callback(**cb_kwargs)
        
def disk_is_empty() -> bool:
    """Return True if no disk is loaded, False otherwise."""
    return __disk is None
    
def play_thread(buffer):
    """Play a sample using simpleaudio."""
    wave_obj = sa.WaveObject(buffer, 1, 2, 44100)
    play_obj = wave_obj.play()
    play_obj.wait_done()
   
def play(num):
    use_loop = bool(get_bool_option('WAVE','use_loop_points',fallback=False)) # Get loop point option
    buffer = __disk.convert_wave(num,use_loop) # create a WAV file from the tone 
    freq = __disk.tones[num].values['FREQUENCY']
    wavedata = __disk.resample(buffer.tobytes(),freq) # resample to 44.1KHz
    thread.start_new_thread(play_thread,(wavedata,)) # Use simpleaudio to play
    
def get_tempdir():
    # Is this ever called?
    return __tempdir
    
def cleanup():
    """
    Clean up temp files after exiting, and shelve any state data.
    """      
    # Clean up temp directory
    __tempdir.cleanup()
    print(f"Cleaned up temporary directory.")
    
    # Save any state information
    db = shelve.open(db_file)
    db['prev_diskdir'] = prev_diskdir
    db['prev_exportdir'] = prev_exportdir
    db.close()    

# Globals section
__disk = None # global disk
# TemporaryDirectory() allows the use of context managers, but for now
# it's best to just let it persist and err on the side of leaving files
# so temporary files can be reused during execution.
hacker_mode = False # Enable / Disable debug
__tempdir = tempfile.TemporaryDirectory()
print(f"Created temporary directory {__tempdir.name}")
version = '0.7.0' # version string

# Path
home_path = pathlib.Path.home()
config_dirs = {
    'Linux': environ.get('$XDG_CONFIG_HOME', os.path.join(home_path,'.config')),
    'freebsd7': environ.get('$XDG_CONFIG_HOME', os.path.join(home_path,'.config')),
    'Darwin': os.path.expanduser('~/Library/Application Support'),
    'Windows': os.path.expanduser('~/AppData/Local')}
config_dir = config_dirs.get(platform.system(), '~')
config_file = os.path.join(config_dir,'rexplore.ini')
db_file = os.path.join(config_dir,'rexplore.db') # for saving state info
config = configparser.ConfigParser()
config.read_file(open(DEFAULTS))
config.read(config_file)
disk_dir = home_path # Variable for default file chooser directory, modified by config
export_dir = home_path # Variable for default file chooser directory, modified by config

# Retrieve shelved path info
db = shelve.open(db_file)
prev_diskdir = db.get('prev_diskdir',home_path) # Previous path chosen for disk images
prev_exportdir = db.get('prev_exportdir',home_path) # Previous path chosen for export
db.close()

def get_option(section: str, key:str, fallback=None):
    """
    Get an option from the config file.
    """
    config.read(config_file)
    value = config.get(section,key,fallback=fallback)
    return value

def get_bool_option(section: str, key:str, fallback=None):
    """
    Get an option from the config file (boolean).
    """
    config.read(config_file)
    value = config.getboolean(section,key,fallback=fallback)
    return value

# Config-related functions
def get_initial_diskdir():
    """Get the initial directory for filechoosers."""

    opt = get_option('FILE','diskdir_opt','Use home directory') # Get mode from config file
    path = home_path # Always default to returning home path
    
    if opt == 'Use home directory':
        # Use home path
        pass
    if opt == 'Remember last directory':
        # Use previous dir
        path = prev_diskdir
    elif opt == 'Use custom':
        # use custom dir
        path = config.get('FILE','custom_diskdir')
    else:
        print('DEBUG: unknown initialdir option')

    return path

def get_initial_exportdir():
    """Get initial export directory for filechoosers."""
    opt = get_option('FILE','exportdir_opt','Use home directory') # Get mode from config file
    path = home_path # Always default to returning home path
    
    if opt == 'Use home directory':
        # Use home path
        pass
    elif opt == 'Remember last directory':
        # Use previous dir
        print(f'DEBUG: using previous')
        path = prev_exportdir
    elif opt == 'Use custom':
        # use custom dir
        path = config.get('FILE','custom_exportdir')
    else:
        print(f'DEBUG: unknown initialdir option {opt}')

    return path

def save_prev_diskdir(filename:str):
    """Save the last chosen directory for filechoosers."""
    global prev_diskdir
    prev_diskdir = os.path.dirname(filename)
        
def save_prev_exportdir(filename:str):
    """Save the last chosen directory for filechoosers."""
    global prev_exportdir
    prev_exportdir = os.path.dirname(filename)


    
