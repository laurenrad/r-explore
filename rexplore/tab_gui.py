"""
R-Explore: tab_gui.py
Module containing notebook tab views for displaying disk information.
"""
import tkinter as tk
from tkinter import ttk
import rexplore.app as app
import rexplore.widgets as w
import rexplore.formatting as formatting
import rexplore.formats as formats
import rexplore.patch_gui as patch_gui
import rexplore.tone_gui as tone_gui

class FunctionFrame(ttk.Frame):
    """
    Frame for displaying function parameters. This is a reworked version that can
    exist empty and reload and reformat itself on demand.
    Commented out code is a solution to adapting for disk format, but backend rework
    is needed before this will actually work.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        # Create label frame with disk label text left justified
        self.disklabel = w.DiskLabelFrame(self, 'dummy')
        self.disklabel.pack(fill=tk.BOTH, expand=1, padx=5)
        
        # Create S-550 params
        self.param_frame_550 = w.FunctionParamFrame(self)
        self.param_frame_550.add_row('MASTER TUNE') # common
        self.param_frame_550.add_row('VOICE MODE') # common
        self.param_frame_550.add_row('KEYBOARD DISPLAY') # S-550
        self.param_frame_550.add_row('EXTERNAL CONTROLLER') # S-550
        self.param_frame_550.pack(fill=tk.BOTH, expand=1, padx=5)
        
        # Create S-50 params
        self.param_frame_50 = w.FunctionParamFrame(self)
        self.param_frame_50.add_row('MASTER TUNE') # common
        self.param_frame_50.add_row('VOICE MODE') # common
        self.param_frame_50.add_row('MIDI CONTROL CHANGE NUMBER') # S-50
        self.param_frame_50.add_row('CONTROLLER ASSIGN') # S-50
        self.param_frame_50.add_row('KEYBOARD ASSIGN') # S-50
        self.param_frame_50.add_row('DP-2 ASSIGN') # S-50
        self.param_frame_50.add_row('AUDIO TRIG') # S-50
        self.param_frame_50.add_row('DT-100') # S-50
        self.param_frame_50.pack(fill=tk.BOTH, expand=1, padx=5)
        
        # Create S-550 multi params

        self.multi_frame_550 = w.ParamTable(self,'function',title='Play')
        self.multi_frame_550.add_row('MULTI MIDI RX-CH')
        self.multi_frame_550.add_row('MULTI PATCH NUMBER')
        self.multi_frame_550.add_row('MULTI LEVEL')
        self.multi_frame_550.pack(fill=tk.BOTH, expand=1, padx=5)
        
        # Create S-550 multi params
        self.multi_frame_50 = w.ParamTable(self,'function',title='Play')
        self.multi_frame_50.add_row('MULTI MIDI RX-CH')
        self.multi_frame_50.add_row('MULTI PATCH NUMBER')
        self.multi_frame_50.add_row('MULTI LEVEL')
        self.multi_frame_50.add_row('MULTI TONE NUMBER')
        self.multi_frame_50.pack(fill=tk.BOTH, expand=1, padx=5)
        
        # grow columns for ParamFrames
        self.param_frame_50.grid_columnconfigure(0,weight=1) # grow columns
        self.param_frame_550.grid_columnconfigure(0,weight=1) # grow columns
        
         
    
    def refresh(self):
        """Reload frame with new disk information after loading a new file."""
        vals = app.get_disk().function.values
        self.disklabel.refresh()
        self.param_frame_50.refresh()
        self.param_frame_550.refresh()
        self.multi_frame_50.refresh()
        self.multi_frame_550.refresh()
        
        self._set_format() # reformat display for disk format

    def _set_format(self):
        """Adjust layout for disk format."""
        if isinstance(app.get_disk_format(),formats.S550):
            self.param_frame_50.pack_forget() # Hide S-50 params frame
            self.multi_frame_50.pack_forget() # Hide S-50 multi params frame
            self.param_frame_550.pack(fill=tk.BOTH, expand=1, padx=5) # Repack S-550 params frame
            self.multi_frame_550.pack(fill=tk.BOTH, expand=1, padx=5) # Repack S-550 multi params frame
        elif isinstance(app.get_disk_format(),formats.S50):
            self.param_frame_550.pack_forget() # Hide S-550 params frame
            self.multi_frame_550.pack_forget() # Hide S-550 multi params frame
            self.param_frame_50.pack(fill=tk.BOTH, expand=1, padx=5) # Repack S-50 params frame
            self.multi_frame_50.pack(fill=tk.BOTH, expand=1, padx=5) # Repack S-50 multi params frame
        else:
            print("DEBUG: bad format in FunctionFrame")
        
class MidiFrame(ttk.Frame):
    """Frame for displaying MIDI parameters"""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        # table to display all parameters that apply to a specific Play channel

        # Common
        self.multi_frame = w.ParamTable(self,'midi',title="MIDI-Message RX")
        self.multi_frame.add_row('RX CHANNEL')
        self.multi_frame.add_row('RX PROGRAM CHANGE')
        self.multi_frame.add_row('RX BENDER')
        self.multi_frame.add_row('RX BEND RANGE')
        self.multi_frame.add_row('RX MODULATION')
        self.multi_frame.add_row('RX HOLD')
        self.multi_frame.add_row('RX AFTER TOUCH')
        self.multi_frame.add_row('RX VOLUME')
        
        # S-50 specific
        self.multi_frame_50 = w.ParamTable(self,'midi',title="Program # RX/TX")
        self.multi_frame_50.add_row('RX PROGRAM NUMBER')
        self.multi_frame_50.add_row('TX PROGRAM NUMBER')
        
        # RX Program Change num (S-550)
        self.progchange_frame = w.ProgChangeFrame(self,title="MIDI-Prog #")
        
        # S-50 specific single value TX parameters
        self.tx_frame = w.MidiParamFrame(self)
        self.tx_frame.add_row('TX CHANNEL')
        self.tx_frame.add_row('TX PROGRAM CHANGE')
        self.tx_frame.add_row('TX BENDER')
        self.tx_frame.add_row('TX MODULATION')
        self.tx_frame.add_row('TX HOLD')
        self.tx_frame.add_row('TX AFTER TOUCH')
        self.tx_frame.add_row('TX VOLUME')
        self.tx_frame.add_row('TX BEND RANGE')
        self.tx_frame.grid_columnconfigure(0,weight=1) # grow columns
        
        # other single MIDI parameters - common to all
        self.param_frame = w.MidiParamFrame(self)
        self.param_frame.add_row('SYSTEM EXCLUSIVE')
        self.param_frame.add_row('DEVICE ID')
        self.param_frame.grid_columnconfigure(0,weight=1) # grow columns     
        
    def refresh(self):
        """Update value labels by refreshing child widgets."""
        self.multi_frame.refresh()
        self.multi_frame_50.refresh()
        self.progchange_frame.refresh()
        self.tx_frame.refresh()
        self.param_frame.refresh()
        self._set_format()
        
    def _set_format(self):
        """Adjust layout for disk format."""
        
        # Unpack everything
        self.multi_frame.pack_forget()
        self.multi_frame_50.pack_forget()
        self.tx_frame.pack_forget()
        self.progchange_frame.pack_forget()
        self.param_frame.pack_forget()
        
        # Repack according to format
        if isinstance(app.get_disk_format(),formats.S550):
            self.multi_frame.pack(fill=tk.BOTH,expand=1,padx=5,pady=5)
            self.progchange_frame.pack(fill=tk.BOTH, expand=1, padx=5) # Repack S-550 stuff
            self.param_frame.pack(fill=tk.BOTH, expand=1, padx=5)
        elif isinstance(app.get_disk_format(),formats.S50):
            self.multi_frame.pack(fill=tk.BOTH,expand=1,padx=5,pady=5)
            self.multi_frame_50.pack(fill=tk.BOTH,expand=1,padx=5)
            self.tx_frame.pack(fill=tk.BOTH,expand=1,padx=5)
            self.param_frame.pack(fill=tk.BOTH, expand=1, padx=5) 
        else:
            print("DEBUG: bad format in MidiFrame")
        
class ListBrowser(ttk.Frame):
    """Merged view for patch and tone tabs."""
    def __init__(self, ItemView, *args, **kwargs):
        """
        Constructor for ListBrowser.
        'ItemView is a tk.Frame view class which can preview a patch or tone
        and implements a refresh method.
        """
        super().__init__(*args,**kwargs)
        self.list_box = tk.Listbox(self,exportselection=False)
        self.list_box.pack(side=tk.LEFT,fill=tk.Y)
        self.list_box.bind('<<ListboxSelect>>',self._on_select) # bind event handler
        self.item_view = ItemView(self)
        self.item_view.pack(side=tk.LEFT,fill=tk.BOTH,expand=True)
        
    def _on_select(self, event):
        """Event handler for item list."""
        index = self.list_box.curselection()[0]
        self.item_view.view_item(index)
        
    def refresh(self):
        self.list_box.delete(0,tk.END) # remove old list items
        
        # add new list items by checking the type of viewer
        if type(self.item_view) is tone_gui.ToneView:
            for t in app.get_disk().tones:
                name = formatting.roland_number(t.number) + ' ' + t.get_name()
                self.list_box.insert(tk.END,name)
        elif type(self.item_view) is patch_gui.PatchView:
            for p in app.get_disk().patches:
                name = formatting.roland_number(p.number) + ' ' + p.get_name()
                self.list_box.insert(tk.END,name)
        else:
            raise RuntimeError('ListBrowser refresh not implemented for this class.')
        
        self.item_view.refresh() # for now just load the first item in the view on refresh
