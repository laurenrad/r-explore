"""
Init module for managing path to config files.
"""
from os import path
import sys

if getattr(sys,'frozen',False):
    DEFAULTS_DIR = path.join(path.dirname(sys.executable),'defaults')
else:
    DEFAULTS_DIR = path.dirname(__file__)
    
DEFAULTS = path.join(DEFAULTS_DIR, 'defaults.ini')
