This is a file tentatively describing the disk format for the Roland S-550 sampler.
This was worked out using the MIDI spec in the manual, a hex editor, and 
a file called "W30.diskinfo" on sgroup.ca containing an archived newsgroup post.
This could use some formatting, verifying, and improvement

Order:

Disk Format:
Name			Size (Bytes)		Start (D)	Start (H)
System			64512			0		0x00000000
Patch Parameter		4096			64512		0x0000FC00			
Function Parameter	224			68608		0x00010C00
MIDI Parameter		288			68832		0x00010CE0*
Tone parameter		4096			69120		0x00010E00
Unknown			512			73216		0x00011E00
Wave Data					73728		0x00012000
----------


System data: 64512 bytes

Patch data @ address 0000FC00 in hex editor:            = 00 00 00 00H (Size shows as 4095 bytes in hex editor)
(addresses are standard hex in GHex)

Offset	Size		Description		Values
0	[12 bytes ]	Patch name		ASCII
12	[1 byte   ]	Bend range		0-12
13	[1 byte   ]	dummy
14	[1 byte   ]	After touch sense	0-127
15	[1 byte   ]	Key mode		0=normal,1=v-sw,2=x-fade,3=v-mix,4=unison
16	[1 byte   ]	Velocity sw threshold	0-127
17	[109 bytes]	Tone to key #1		0-31  (Note: this is the first of two tones that can be assigned to each note. Each of the 109 bytes is one of the possible notes)
126	[109 bytes]	Tone to key #2		0-31
235	[1 byte   ]	copy source		0-7
236	[1 byte   ]	octave shift*		-2-+2 (Note: i have no idea yet how this is encoded, is it ascii or a signed 8 bit number)
237	[1 byte   ]	output level		0-127
238	[1 byte   ]	dummy			(Note: this one was set to 0x40 on 5501_01 for some reason)
239	[1 byte   ]	detune*			-64-+63 (again don't know how this is encoded. and is it "unison detune"?)
240	[1 byte   ]	velocity mix ratio*	0-127
241	[1 byte   ]	after touch assign	0=modulation,1=volume,2=bend+,3=bend-,4=filter ("cut-off")
242	[1 byte   ]	key assign		0=rotary,1=fix	
243	[1 byte   ]	output assign		0-8, where 8=tone and 0-7 are outs 1-8
244	[12 bytes ]	dummy (Ends at FD00) 
x16 (S550 has 16 patches per block, S-50 has 8 patches and only 1 block). Disk stores 1 block, so 16 patches for S-550
1 patch block occupies 256 bytes.


0x00010C00: Function parameters:
	[1 byte]	master tune		-64-+63
10C00	[13 bytes]	dummy
10C0D	[1 byte]	dummy
10C0E	[1 byte]	dummy (15 total here)
10C0F	[1 byte]	voice mode		0=last note priority,1=first note priority,2-23=fix mode 1-22
10C10	[8 bytes]	multi midi rx ch 1-8	0-15=midi channels 1-16, 0x10="OFF". The latter isn't documented in the midi spec. Each byte represents 1 of the 8 performance channels on the play screen
	[8 bytes]	multi patch number 1-8	0-31
10C20	[9 bytes]	dummy
	[1 byte]	keyboard display	0-7=A-H,8=all (*This is a bit cut off in the manual and not clear at all, the disk i'm looking at has 0x00 and is set to standard/none, it's not clear how that's represented)
	[8 bytes]	multi level		1-127 (volume of each of 8 channels)
10C32	[60 bytes]	disk label		ASCII; for some reason the first line is just the first 12 bytes and then afterwards it starts printing by columns.
10C6E	[4 bytes]	dummy
	[1 byte]	external controller	0=off,1=mouse,2=rc-100
	[108 bytes]	dummy
	
0x00010CE0 (68832): MIDI Parameter
	[64 bytes]	dummy
	[8 bytes ]	RX Channel 1-8		0-15=Channel 1-16, 16=off
	[8 bytes ]	RX Program Change 1-8	0=OFF,1=ON
10D30	[8 bytes ]	RX Bender 1-8		0=OFF,1=ON
	[8 bytes ]	RX Modulation 1-8	0=OFF,1=ON
10D40	[8 bytes ]	RX Hold 1-8		0=OFF,1=ON
	[8 bytes ]	RX After touch		0=OFF,1=ON
10D50	[8 bytes ]	RX Volume		0=OFF,1=ON	
	[8 bytes ]	RX Bend range		0=OFF,1=ON
10D60	[1 byte  ]	Dummy			
	[1 byte  ]	System Exclusive	0=OFF,1=ON
	[1 byte  ]	Device ID		0-15
	[32 bytes]	RX Prog. Change No. 1-3	0-127
10D83	[125 bytes]	Dummy

0x00010E00 (69120): Tone parameters
Size		Key Name		Possible Values			Description
-------------------------------------------------------------------------------------------------------------------------
[8  bytes]	Name			ASCII, 8 chars			tone name
[1  byte ]	output_assign		0-7				output assign
[1  byte ]	source_tone		0-31 ("Original tone")		source tone
[1  byte ]	subtone			0=ORG,1=SUB			org/sub tone
[1  byte ]	frequency		0=30kHz,1=15kHz			sampling rate
[1  byte ]	orignal_key		11-120 (MIDI FORMAT)		original key
[1  byte ]	wave_bank		0=A,1=B				wave bank
[1  byte ]	segment_top		0-17				index of first segment containing wave data
[1  byte ]	wave_length		0-18				number of segments occupied by wave data
[3  bytes]	start_point		000000-221180			start point
[3  bytes]	end_point		000004-221184			end point
[3  bytes]	loop_point						loop point
[1  byte ]	loop_mode		0=fwd,1=alt,2=1shot,3=reverse	loop mode
--------------------------------------------------------------------------------------------------------------------------
[1  byte ]	TVA LFO depth		0-127
[1  byte ]	dummy
[1  byte ]	LFO rate		0-127
[1  byte ]	LFO sync		0=off,1=on
[1  byte ]	LFO delay		0-127	
[1  byte ]	dummy			
[1  byte ]	LFO Mode		0=NORMAL,1=ONE SHOT * (Can't find this in the menus. This is not "Mode" under LFO, which is actually LFO POLARITY)
[1  byte ]	OSC LFO DEPTH		0-12
[1  byte ]	LFO polarity		0=Sine,1=Peak hold
[1  byte ]	LFO offset		0-127
[1  byte ]	Transpose		0-127 (Should be "Shift?")
[1  byte ]	Fine tune		-64-+63 (signed 8 bit value)
[1  byte ]	TVF Cutoff		0-127
[1  byte ]	TVF Resonance		0-127
[1  byte ]	TVF Key Follow		0-127
[1  byte ]	dummy
[1  byte ]	TVF LFO Depth		0-127
[1  byte ]	TVF EG Depth		0-127
[1  byte ]	TVF EG Polarity		0=NORMAL (positive), 1=REVERSE (negative)
[1  byte ]	TVF Level Curve		0-5
[1  byte ]	TVF Key rate follow	0-127 ("Key-Rate")
[1  byte ]	TVF Vel Rate follow	0-127
[1  byte ]	dummy			(set to 0 on here, may be "Disp.Zoom on TVF?")
[1  byte ]	TVF Switch		0=OFF,1=ON ("TVF" under "Tone PRM")
[1  byte ]	Bender switch		0=OFF,1=ON ("Pitch Bender" under "Tone PRM")
[1  byte ]	TVA Env Sustain Point	0-7 ("Sus" on TVA screen)
[1  byte ]	TVA Env End point	1-7 ("End" on TVA screen)
[1  byte ]	TVA Env Level 1		1-127
[1  byte ]	TVA Env Rate 1		1-127
[1  byte ]	TVA Env Level 2		1-127
[1  byte ]	TVA Env Rate 2		1-127
[1  byte ]	TVA Env Level 3		1-127
[1  byte ]	TVA Env Rate 3		1-127
[1  byte ]	TVA Env Level 4		1-127
[1  byte ]	TVA Env Rate 4		1-127
[1  byte ]	TVA Env Level 5		1-127
[1  byte ]	TVA Env Rate 5		1-127
[1  byte ]	TVA Env Level 6		1-127
[1  byte ]	TVA Env Rate 6		1-127
[1  byte ]	TVA Env Level 7		1-127
[1  byte ]	TVA Env Rate 7		1-127
[1  byte ]	TVA Env Level 8		1-127
[1  byte ]	TVA Env Rate 8		1-127
[1  byte ]	dummy	
[1  byte ]	TVA ENV KEY-RATE	0-127
[1  byte ]	LEVEL			0-127 (Presumably tone level)
[1  byte ]	ENV VEL-RATE		0-127 ("Vel-Rate" under TVA screen)
[1  byte ]	REC THRESHOLD		0-127 (Interesting that this and the other related ones below are stored since it only comes up when recording waves)
[1  byte ]	REC PRE-TRIGER (sic)	0=0ms,1=10ms,2=50ms,3=100ms
[1  byte ]	REC SAMPLING FREQUENCY	0=30kHz, 1=15kHz
[3  bytes]	REC START POINT		000000-221180
[3  bytes]	REC END POINT		000004-221184
[3  bytes]	REC LOOP POINT		000000-221184 (Not sure what this is for but maybe "Wave Loop" utility?
[1  byte ]	ZOOM T			0-5 (???)
[1  byte ]	ZOOM L?			??? (Scan is cut off here. Set to 1 for E.Bass 3)	
[1  byte ]	COPY SOURCE		0-31
[1  byte ]	LOOP TUNE		-64-+63 (On Loop screen)
[1  byte ]	TVA LEVEL CURVE		0-5 (On TVA screen)
[12 bytes]	dummy	
[3 bytes]	LOOP LENGTH		000004-221184 (Seem to just be for display on loop screen)
[1 byte ]	PITCH FOLLOW		0=OFF,1=ON	
[1 byte ]	ENV ZOOM		0-5 (One of the "Disp.Zoom"s but it's not clear if it's TVA or TVF since there's only one but they can be changed independently)
[1 byte ]	TVF ENV SUSTAIN POINT	0-7
[1 byte ]	TVF ENV END POINT	1-7
[1 byte ]	TVF ENV LEVEL 1		0-127
[1 byte ]	TVF ENV RATE 1		0-127
[1 byte ]	TVF ENV LEVEL 2		0-127
[1 byte ]	TVF ENV RATE 2		0-127
[1 byte ]	TVF ENV LEVEL 3		0-127
[1 byte ]	TVF ENV RATE 3		0-127
[1 byte ]	TVF ENV LEVEL 4		0-127
[1 byte ]	TVF ENV RATE 4		0-127
[1 byte ]	TVF ENV LEVEL 5		0-127
[1 byte ]	TVF ENV RATE 5		0-127
[1 byte ]	TVF ENV LEVEL 6		0-127
[1 byte ]	TVF ENV RATE 6		0-127
[1 byte ]	TVF ENV LEVEL 7		0-127
[1 byte ]	TVF ENV RATE 7		0-127
[1 byte ]	TVF ENV LEVEL 8		0-127
[1 byte ]	TVF ENV RATE 8		0-127
[1 byte ]	AFTER TOUCH SWITCH	0=OFF,1=ON
[2 bytes]	dummy
size of single tone data: 
	
?? 512 bytes @ 73216 (0x00011E00)

Wave Data starts at 0x00012000
12288 samples per segment (see loop screen in interface)
18432 bytes per segment (see "W30.diskinfo") * 18 segments = 331776
Note: W30.diskinfo says 18342 bytes per segment, but if you note that a segment is listed as 12288 samples long on the interface, and a sample takes up 1.5 bytes (which are packed into 3 byte chunks with another sample), you get 18432, and multiplying this by 18 segments and 2 banks, you get 663552 bytes, which appears to be the amount of wave data showing in the hex editor. It's not clear if this is just a typo or perhaps a difference in the spec between the S-550 and W30.
NOTE: I believe the "18342 bytes per segment in the old newsgroup posting i used to figure out info may be a type, it seems it may be 18432; double check this later.
on this disk format. it lists "18342" bytes per segment.
331776 * 2 wave banks = 663552 bytes (663.552K)
	
	
734040
