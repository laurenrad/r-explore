"""
Init module for managing path to documentation.
"""
from os import path
import sys

if getattr(sys,'frozen',False):
    DOC_DIR = path.join(path.dirname(sys.executable),'doc')
else:
    DOC_DIR = path.dirname(__file__)

README = path.join(DOC_DIR, 'readme.txt')
