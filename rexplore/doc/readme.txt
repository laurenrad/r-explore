R-Explore - A sample disk browser for Roland 12-bit samplers
------------------------------------------------------------

========================
Features
========================
- Play back wave samples
- Export single wave sample, or export all samples to a zip archive
- Export a text report containing parameter information
- Browse parameters (see 'Format Support' section)

========================
What It Doesn't Do
========================
- Edit disk data
- Read in disk images; a sample disk must first be imaged with another program
  such as SDISKW or dd.

========================
Usage
========================
When R-Explore first starts, most of the interface is disabled. Open a disk
image with 'File -> Open Disk Image' or Ctrl+O. The current disk image will be
displayed at the bottom of the main window, along with the version number of
the system software stored on the disk.
The main interface is split into four tabs:

Function: This corresponds to data which is displayed in the Play mode of the
sampler, Function or Disk mode, including the Disk Label.

MIDI: This corresponds to data displayed in the MIDI mode of the sampler.

Patches: This displays a list of patches and their associated parameter data.
To view a patch, select it in the list on the left and the data for the patch
will be displayed in the view frame on the right.
Since patches can contain multiple tones, there is no play function for this
tab. The 'Linked Tones' list displays all tones which are assigned to at least
one key in a patch.

Tones: Similarly to the patch browser, this allows you to view the parameters
associated with tones. The tabs at the top of the parameter view allow you to
select a parameter category to view.

Below the parameter view is a display of a tone's associated waveform, along
with its loop points. The Start and End points will be used for playback and
export if 'Use start and end points for play and export' is selected in the
Options window. The Loop point is displayed, but not used.

Below the waveform view are buttons for playing and exporting a tone's wave
data. Again, the Start and End points can be applied to these buttons if the
appropriate option is chosen.
Tone wave data is exported to a WAV file (16-bit, 44.1kHz).

All samples can be exported at once to a zip archive with the menu option
'File -> Export All Samples'.

The menu option 'File -> Export Disk Report' will output parameters from disk
to a text file; note that this reflects how the parameters are read into the
program, which is fairly close to how they're stored on disk. So some
parameters will not reflect what's displayed on the sampler, as they're stored
numbered from 0, etc.

========================
Options
========================
The Options window (File->Options) provides the following options:

- Default disk directory
    This controls which directory will be opened by default when choosing a
    disk image. 'Remember last directory' will use the last directory chosen,
    even across sections. 'Use home directory' will use your home or user
    directory, which will vary across platforms. 'Use custom' allows you to
    choose a default starting directory where you typically store disk iamges.
- Default export directory
    This option is the same as above except for file dialogs when exporting a
    sample, exporting all samples, or exporting a disk report.
- Use start and end points for play and export
    When this option is checked, the start and end points associated with a
    tone will be used when playing back and exporting the tone's wave data.

========================
Keyboard Shortcuts
========================
Windows/Linux:
Ctrl + O: Open disk image
Ctrl + E: Export all samples
Ctrl + S: Export disk report
Ctrl + Q: Quit R-Explore
Ctrl + H: Display this help

macOS:
Command + O: Open Disk Image
Command + ,: Open Preferences
Command + E: Export all samples
Command + S: Export disk report
Command + Q: Quit R-Explore

========================
Platform Support
========================
R-Explore is still in early development; it was developed on Linux Mint 20 with
Python 3.8, so support is mainly verified for this platform. However, it has
tested fine on Windows and macOS and should run on any major platform which
supports a recent version of Python. The only external library dependency is
simpleaudio. R-Explore does not yet integrate well with macOS.

========================
Format Support
========================
R-Explore supports the following systems:
Roland S-50
Roland S-550
Roland S-330

========================
Known Issues
========================
- Occasional minor distortion on tone playback

========================
Copying and Author
========================
This program is released under the terms of the MIT License and comes with no
warranty. For more information, see the LICENSE file. This program still needs
extensive testing, so if you run into any issues or have questions contact
the author, Lauren Croney, at laurenc.ist.rad@gmail.com (especially if you have
an S-50 or S-330).
