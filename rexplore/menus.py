"""
R-Explore: menus.py
Module for toplevel menus.
"""
import tkinter as tk
from functools import partial

class MenuBar(tk.Menu):
    """Parent class for platform-specific menu bar classes."""
    
    @staticmethod
    def _argstrip(function, *args):
        """Strip arguments from callbacks."""
        return function()

    def enable_debug(self):
        """Enable the DEBUG menu."""
        debug_keybindings = { '<Control-t>': self.callbacks['debug->testfile'],
                              '<Control-r>': self.callbacks['debug->testfile2'] }
        self._bind_accelerators(debug_keybindings) # Apply DEBUG keybindings
        self.debug_menu.add_command(label='The Test',command=self.callbacks['debug->test'])
        self.debug_menu.add_separator()
        self.debug_menu.add_command(label='Open test file (S-50)',command=self.callbacks['debug->testfile'],
                                   accelerator='Ctrl+T')
        self.debug_menu.add_command(label='Open test file (S-550)',command=self.callbacks['debug->testfile2'],
                                   accelerator='Ctrl+R')
        self.add_cascade(label="DEBUG",menu=self.debug_menu)
    
    def _bind_accelerators(self, keybinds):
        """Bind global key shortcuts."""
        for key, command in keybinds.items():
            self.bind_all(key, partial(self._argstrip, command))

class MainMenuMac(MenuBar, tk.Menu):
    """Toplevel menu bar for macOS."""
    def __init__(self, parent, callbacks, **kwargs):
        super().__init__(parent,**kwargs)
        
        # store callbacks
        self.callbacks = callbacks

        # set key accelerator bindings (platform-sepecific)
        modifier = 'Meta' # If Mac though, set modifier to Meta (Command)
        self._bind_accelerators(self.get_keybindings())

        # create app menu
        self.app_menu = tk.Menu(self, name='apple')
        self.app_menu.add_command(label='About R-Explore',command=self.callbacks['help->about'])
        parent.createcommand('tk::mac::ShowPreferences',
                             self.callbacks['file->options']) # Add preferences option
        parent.createcommand('exit',
                             self.callbacks['file->quit']) # Override quit

        # create help menu
        self.help_menu = tk.Menu(self, name='help')
        parent.createcommand('tk::mac::ShowHelp',callbacks['help->view'])

        # create window menu
        self.window_menu = tk.Menu(self, name='window')

        # create other menus
        self.file_menu = tk.Menu(self, tearoff=False)
        self.debug_menu = tk.Menu(self, tearoff=False)
        self.helpmenu = tk.Menu(self, tearoff=False)

        
        # add file menu entries            
        self.file_menu.add_command(label='Open Disk Image',accelerator=(modifier+'+O'),
                                   command=callbacks['file->open'])
        # example if i want to use events instead:
        #m_edit.add_command(label="Paste", command=lambda: root.focus_get().event_generate("<<Paste>>"))


        self.file_menu.add_command(label='Export All Samples',command=callbacks['file->exportall'],
                                   accelerator=('Command+E'), state=tk.DISABLED) # only enable if a disk is open
        self.file_menu.add_command(label='Export Disk Report',command=callbacks['file->exportreport'],
                                   accelerator=('Command+S'), state=tk.DISABLED) # same as above
        
        # add all to toplevel
        self.add_cascade(label='R-Explore',menu=self.app_menu)
        self.add_cascade(label="File", menu=self.file_menu)
        self.add_cascade(label="Window",menu=self.window_menu)
        self.add_cascade(label="Help",menu=self.help_menu)

    def get_keybindings(self):
        """Return a dict containing global key bindings for macOS (excepting DEBUG)."""
        # This does not bind quit because it's redefining the builtin in the constructor
        return { '<Command-o>': self.callbacks['file->open'],
                 '<Command-e>': self.callbacks['file->exportall'],
                 '<Command-s>': self.callbacks['file->exportreport']}


class MainMenu(MenuBar, tk.Menu):
    """Toplevel menu bar."""
    def __init__(self, parent, callbacks, **kwargs):
        super().__init__(parent,**kwargs)
        
        # store callbacks
        self.callbacks = callbacks

        # set key accelerator bindings (platform-sepecific)
        modifier = 'Ctrl'
        self._bind_accelerators(self.get_keybindings()) # bind key accelerators

        self.file_menu = tk.Menu(self, tearoff=False)
        self.debug_menu = tk.Menu(self, tearoff=False)
        self.help_menu = tk.Menu(self, tearoff=False)

        
        # add file menu entries            
        self.file_menu.add_command(label='Open Disk Image',command=callbacks['file->open'],
                                   accelerator=(modifier+'+O'))

        self.file_menu.add_command(label='Export All Samples',command=callbacks['file->exportall'],
                                   accelerator=(modifier+'+E'), state=tk.DISABLED) # only enable if a disk is open
        self.file_menu.add_command(label='Export Disk Report',command=callbacks['file->exportreport'],
                                   accelerator=(modifier+'+S'), state=tk.DISABLED) # same as above
        self.file_menu.add_command(label='Options',command=callbacks['file->options'])
        self.file_menu.add_separator()
        self.file_menu.add_command(label='Quit',command=callbacks['file->quit'],
                                   accelerator=('Ctrl+Q'))
        
        # add help menu entries
        self.help_menu.add_command(label='View help',command=callbacks['help->view'],
                                   accelerator=('Ctrl+H'))
        self.help_menu.add_command(label='About R-Explore',command=callbacks['help->about'])
        
        # add all to toplevel
        self.add_cascade(label="File", menu=self.file_menu)
        self.add_cascade(label="Help",menu=self.help_menu)
    
    def get_keybindings(self):
        """
        Return a dict containing global key bindings for all
        non-Mac platforms (excepting DEBUG).
        """
        return { '<Control-o>': self.callbacks['file->open'],
                 '<Control-e>': self.callbacks['file->exportall'],
                 '<Control-s>': self.callbacks['file->exportreport'],
                 '<Control-q>': self.callbacks['file->quit'],
                 '<Control-h>': self.callbacks['help->view'] }
