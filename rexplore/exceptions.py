# exceptions.py
class FormatError(Exception):
    """
    Exception raised on invalid disk format.
    TODO: full implementation
    """
    def __str__(self):
        return "FormatError"

class FileSizeError(Exception):
    """
    Exception raised on attempting to load a file that is too large.
    """
    def __str__(self):
        return "FileSizeError"