"""
R-Explore: patch_gui.py
Patch detail view window.
"""

import tkinter as tk
from tkinter import ttk
import rexplore.widgets as w
import rexplore.formatting as formatting
import rexplore.formats as formats
from rexplore.formatting import display_names # Display names of parameters
import rexplore.app as app

class PatchView(ttk.Frame):
    """Displays information about a single patch at a time."""
    
    # Fields to display for S-550
    patch_fields_550 = ['NAME',
                        'BEND RANGE',
                        'AFTER TOUCH SENSE',
                        'KEY MODE',
                        'VELOCITY SW THRESHOLD',
                        'OCTAVE SHIFT',
                        'OUTPUT LEVEL',
                        'DETUNE',
                        'VELOCITY MIX RATIO',
                        'AFTER TOUCH ASSIGN',
                        'KEY ASSIGN',
                        'OUTPUT ASSIGN']
    # Fields to display for S-50
    patch_fields_50 = ['NAME',
                        'BEND RANGE',
                        'AFTER TOUCH SENSE',
                        'KEY MODE',
                        'VELOCITY SW THRESHOLD',
                        'OCTAVE SHIFT',
                        'OUTPUT LEVEL',
                        'MODULATION DEPTH',
                        'DETUNE',
                        'VELOCITY MIX RATIO',
                        'AFTER TOUCH ASSIGN']
    
    def __init__(self,parent,*args,**kwargs):
        super().__init__(parent,*args,**kwargs)
        self.number = 0 # patch number to display
        
        self.params_frame_50 = w.PatchParamFrame(self,borderwidth=2,relief="ridge") # create frame to display parameters
        for field in self.patch_fields_50:
            self.params_frame_50.add_row(field) # add empty parameter views to the frame
        self.params_frame_50.pack(fill=tk.BOTH, expand=1, padx=5)
        self.params_frame_50.grid_columnconfigure(0,weight=1) # grow columns
        
        self.params_frame_550 = w.PatchParamFrame(self,borderwidth=2,relief="ridge") # S-550 version
        for field in self.patch_fields_550:
            self.params_frame_550.add_row(field)
        self.params_frame_550.pack(fill=tk.BOTH,expand=1,padx=5)
        self.params_frame_550.grid_columnconfigure(0,weight=1)

        self.tone_assign_frame = ToneAssignFrame(self,text="Tone Assignment") # same for all formats
        
    def refresh(self):
        """Update values and change layout; called on new file open."""
        self.view_item(0) # update values
        self.update_layout() # update layout
        
    def view_item(self,num):
        """Show values for a newly selected tone without changing layout."""
        self.number = num
        self.params_frame_50.refresh(num)
        self.params_frame_550.refresh(num)
        self.tone_assign_frame.refresh(num)    
        
    def update_layout(self):
        """
        Adjust layout for disk format.
        """
        self.tone_assign_frame.pack_forget()
        if isinstance(app.get_disk_format(),formats.S50):
            self.params_frame_550.pack_forget() # remove S-550 frame
            self.params_frame_50.pack(fill=tk.BOTH,expand=1,padx=5) # repack S-50 frame
        elif isinstance(app.get_disk_format(),formats.S550):
            self.params_frame_50.pack_forget()
            self.params_frame_550.pack(fill=tk.BOTH,expand=1,padx=5)
        self.tone_assign_frame.pack(fill=tk.BOTH,expand=1,padx=5)

class ToneAssignFrame(ttk.LabelFrame):
    """
    LabelFrame for displaying tone to key assignment.
    Needs work.
    """
    def __init__(self,parent,*args,**kwargs):
        super().__init__(parent,*args,**kwargs)
        # D8 is the highest value the interface actually seems to interact with,
        # but more is stored so since this is temporary it will just do the whole thing.
        self.number = 0 # patch number
        self.note_name = tk.StringVar() # selected note
        self.tone1 = tk.StringVar() # variable for 1st tone assigned to a note
        self.tone2 = tk.StringVar() # variable for 2nd tone assigned to a note

        self.select_scale = tk.Scale(self,orient=tk.HORIZONTAL,from_=12,to=120,
                                     command=self._on_note_select,showvalue=False)
        self.select_scale.grid(row=1,column=0,sticky=tk.EW)
        ttk.Label(self,textvariable=self.note_name).grid(row=1,column=1,sticky=tk.W)
        ttk.Label(self,text="1st tone: ").grid(row=2,column=0,sticky=tk.W)
        ttk.Label(self,textvariable=self.tone1,anchor=tk.E).grid(row=2,column=1,sticky=tk.W)
        ttk.Label(self,text="2nd tone: ").grid(row=3,column=0,sticky=tk.W)
        ttk.Label(self,textvariable=self.tone2,width=12,anchor=tk.W).grid(row=3,column=1,sticky=tk.W)
        
        # Tone list
        ttk.Label(self,text="").grid(row=4,column=0) # Separator row
        ttk.Label(self,text="Linked Tones:").grid(row=5,column=0,sticky=tk.W)
        self.tone_list = tk.Listbox(self,exportselection=False)
        self.tone_list.grid(row=6,column=0,columnspan=2,sticky='NSEW')
        self.list_scroll = ttk.Scrollbar(self)
        self.list_scroll.grid(row=6,column=3,sticky=(tk.N+tk.S))
        self.tone_list.config(yscrollcommand = self.list_scroll.set)
        self.list_scroll.config(command = self.tone_list.yview)
        
        # Set tone list to grow
        self.grid_rowconfigure(6,weight=1)
        self.grid_columnconfigure(0,weight=1)
        self.grid_columnconfigure(1,weight=1)
        self.grid_columnconfigure(3,weight=0)
        
        
    def _on_note_select(self,event):
        """Callback for the temporary tone to key widget."""
        values = app.get_disk().patches[self.number].values.copy() # get shallow copy
        note = self.select_scale.get()
        self.note_name.set(formatting.int_to_note(note))
        tone_num1 = values['TONE TO KEY 1'][note-12] # index of the first tone assigned
        tone_num2 = values['TONE TO KEY 2'][note-12] # index of the first tone assigned
        try:
            self.tone1.set((formatting.roland_number(tone_num1) + ' ' + app.get_tone_name(tone_num1)).strip())
            self.tone2.set((formatting.roland_number(tone_num2) + ' ' + app.get_tone_name(tone_num2)).strip())
        except Exception as E:
            # just set -- to show empty; if there is an exception, details will be printed on refresh
            self.tone1.set('--') 
            self.tone2.set('--')
        
    def refresh(self,num):
        """
        Update view when a new patch is selected.
        """
        self.number = num
        self._on_note_select(12)
        
        # Update tone list
        self.tone_list.delete(0,tk.END) # remove old list items
        tones = app.get_disk().patches[self.number].tone_list() # get linked tones
        for t in tones:
            try:
                name = formatting.roland_number(t) + ' ' + app.get_tone_name(t)
            except Exception as E:
                print(f'Exception getting tone information: ({E})')
            finally:                
                self.tone_list.insert(tk.END,name)
        
