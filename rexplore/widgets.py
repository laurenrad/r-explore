"""
R-Explore: widgets.py
This module contains custom widget classes for use by the R-Explore GUI.
"""
from abc import ABCMeta, abstractmethod
import tkinter as tk
from tkinter import ttk
import rexplore.formatting as formatting
import rexplore.app as app
import rexplore.formats
from .images import * # icons

font_bold = 'TkDefaultFont 10 bold'

class Dropdown(ttk.Combobox):
    """A Combobox class which acts more as a traditional dropdown widget."""
    def __init__(self,parent,*args,**kwargs):
        super().__init__(parent,*args,**kwargs)
        self.config(state='readonly')
        self.bind('<<ComboboxSelected>>', self._on_select)

    def _on_select(self,event):
        self.selection_clear() # Deselect text when making a selection
        print(f'Value: {self.get()} [{event}]')

class ParamRow:
    """
    Parent class for storing and recalling data representing a global parameter
    row, for use by ParamFrame and similar classes.
    """
    def __init__(self, key:str):
        self.key = key # Key name of parameter
        self.var = tk.StringVar() # Backing textvariable
        
    def set(self, key, value):
        """Update the StringVar for this row."""
        if value is None:
            self.var.set('') # no value; this is normal when no file is open
        else:
            self.var.set(formatting.format_value(key,value)) # set the value text label
        
class ParamFrame(ttk.Frame, metaclass=ABCMeta):
    """
    Abstract class for displaying rows of parameter keys and values.
    Needs a refresh method to be implemented which reloads the values.
    """
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.rows = [] # initialize list of tuples containing row information
    
    def add_row(self, key:str):
        """
        Add a row containing a parameter to the frame.
        'key' is the parameter to track.
        """
        row = ParamRow(key)
        key_text = formatting.display_names.get(key,key) # default to key name if no display name
        row.set(key,None)
        
        key_label = ttk.Label(self,text=key_text)
        key_label.config(font='TkDefaultFont 10 bold')
        key_label.grid(row=len(self.rows),column=0,sticky=tk.W)
        value_label = ttk.Label(self,textvariable=row.var)
        value_label.grid(row=len(self.rows),column=1,sticky=tk.E)
        
        self.rows.append(row) # Store row information to be updated later
    
    @abstractmethod
    def refresh(self):
        """Reload values."""
        pass
    
class FunctionParamFrame(ParamFrame, ttk.Frame):
    """
    ParamFrame subclass for Function parameters.
    """
    def refresh(self):
        """
        Update all value labels.
        """
        for row in self.rows:
            value = app.get_function_value(row.key)
            row.var.set(str(formatting.format_value(row.key,value)))
            
class MidiParamFrame(ParamFrame, ttk.Frame):
    """
    ParamFrame subclass for MIDI parameters.
    """
    def refresh(self):
        """
        Update all value labels.
        """
        for row in self.rows:
            value = app.get_midi_value(row.key)
            row.var.set(str(formatting.format_value(row.key,value)))

class PatchParamFrame(ParamFrame, tk.Frame):
    """
    Frame class for displaying rows of parameter keys and values.
    """
        
    def refresh(self,num):
        """
        Update all value labels for a given patch.
        """
        for row in self.rows:
            value = app.get_patch_value(row.key,num)
            row.var.set(str(formatting.format_value(row.key,value)))

class ToneParamFrame(ParamFrame, ttk.Frame):
    """
    Frame class for displaying rows of parameter keys and values.
    """ 
    def refresh(self,num):
        """
        Update all value labels for a given patch.
        """
        for row in self.rows:
            if (row.key == 'SOURCE TONE' and
                app.get_tone_value('ORIG/SUB TONE',num) == 1):
                # special handling: source tone shouldn't be displayed if this isn't a subtone
                row.var.set("***")
            else:
                value = app.get_tone_value(row.key,num)
                row.var.set(str(formatting.format_value(row.key,value)))
                
class TableFrame(ttk.Frame):
    """Frame class for displaying a table of values."""
    def __init__(self, parent, title=' ', columns=8, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.col_count = columns # Number of columns
        self.row_count = 0 # Number of rows including header
        self.title = title
        self.rows = {} # Dictionary storing row variables by key
        self.bw = 2 # Border width
        self.style = "ridge" # Border style
        self.vars = [] # Variables for cell values
        self.col_vars = [] # Variables for column header values
        for i in range(columns):
            self.col_vars.append(tk.StringVar())
        self._create_header()
        
    def _create_header(self):
        # Create table header
        self.title_label = ttk.Label(self,text=self.title) # Title cell
        self.title_label.config(font=font_bold)
        self.title_label.grid(row=0,column=0,sticky=(tk.E+tk.W))
        for col in range(self.col_count):
            lbl = ttk.Label(self,textvariable=self.col_vars[col],
                           borderwidth=self.bw,relief=self.style)
            lbl.config(font=font_bold)
            lbl.grid(row=0,column=col+1,sticky=(tk.E+tk.W)) # Centered
            self.grid_columnconfigure(col+1,weight=1) # Expand cell
        self.grid_columnconfigure(0,weight=2) # Expand title with higher priority
        self.row_count += 1
        
    def set_col_names(self, header=['A','B','C','D','E','F','G','H']):
        """Set column names."""
        for (col,text) in enumerate(header):
            self.col_vars[col].set(text)
            
    def add_row(self, key:str):
        """Add a row to the table."""
        row_vars = [tk.StringVar() for x in range(self.col_count)]
        self.rows[key] = row_vars # Store new row of StringVars
        row_name = formatting.display_names.get(key,key) # formatted parameter name
        lbl = ttk.Label(self,text=row_name,borderwidth=self.bw,
                       relief=self.style,anchor=tk.W)
        lbl.config(font=font_bold)
        lbl.grid(row=self.row_count,column=0,sticky=(tk.E+tk.W))
        
        for col in range(0,self.col_count):
            ttk.Label(self,
                     textvariable=self.rows[key][col],
                     borderwidth=self.bw,relief=self.style).grid(row=self.row_count,
                                                            column=col+1,sticky=(tk.E+tk.W))
        self.row_count += 1
        
    def set_row(self, key, values):
        """Manually set values for a row."""
        if len(values) != self.col_count:
            raise RuntimeError('Incorrect number of values for this row')
        else:
            for (i, value) in enumerate(values):
                try:
                    self.rows[key][i].set(values[i])
                except IndexError as E:
                    print(f"Size mismatch while setting multi param row. ({E})")
        
class ProgChangeFrame(TableFrame):
    """Frame class for displaying the RX Program Change # Table."""
    def __init__(self, parent, title=' ', *args, **kwargs):
        TableFrame.__init__(self,parent,title,8,*args,**kwargs) # Call TableFrame constructor
        self.set_col_names()
        self.add_row('I1')
        self.add_row('I2')
        self.add_row('II1')
        self.add_row('II2')
                
    def refresh(self):
        """Update value labels."""
        vals = app.get_midi_value('RX PROGRAM CHANGE NUMBER')
        if vals: # If we didn't get any values back just ignore, this is probably not S-550
            # split up across four rows
            self.set_row('I1',vals[0:8])
            self.set_row('I2',vals[8:16])
            self.set_row('II1',vals[16:24])
            self.set_row('II2',vals[24:32])
                    
class ParamTable(TableFrame):
    """LabelFrame for displaying per channel (A-H) parameters."""
    def __init__(self, parent, location, title=' ',  *args, **kwargs):
        TableFrame.__init__(self,parent,title,8,*args,**kwargs) # Call TableFrame constructor
        self.set_col_names()
        self.location = location
        
    def refresh(self):
        for key in self.rows.keys():
            if self.location == 'function':
                values = formatting.format_value(key,app.get_function_value(key))
            elif self.location == 'midi':
                values = formatting.format_value(key,app.get_midi_value(key))
            else:
                # This error check should be moved but this needs to be reworked anyway.
                raise RuntimeError('Bad location refreshing global row')
            
            if values:
                self.set_row(key,values)
            else:
                # If values is None, just set all values in row to None
                self.set_row(key,[None for v in range(self.col_count)])
            
class EnvelopeView(ttk.LabelFrame):
    """Frame class for displaying envelope data."""
    def __init__(self, parent, mode, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        bold = 'TkDefaultFont 10 bold'
        
        self.mode = mode # 'TVF' or 'TVA'
        
        # Create sustain and end labels
        self.top_table = ttk.Frame(self)
        self.sustain = tk.StringVar() # Envelope sustain value
        self.endpoint = tk.StringVar() # Envelope end point
        # this is coded to TVF but the binding is the same
        sus_label = ttk.Label(self.top_table,text='Sus:')
        sus_label.config(font=bold)
        sus_label.grid(row=0,column=0)
        ttk.Label(self.top_table,textvariable=self.sustain).grid(row=0,column=1,
                                                                padx=10)
        end_label = ttk.Label(self.top_table,text='End:')
        end_label.config(font=bold)
        end_label.grid(row=0,column=2)
        ttk.Label(self.top_table,textvariable=self.endpoint).grid(row=0,column=3,
                                                                 padx=10)
        for i in range(0,4): # Expand columns evenly
            self.top_table.grid_columnconfigure(i,weight=1,uniform="top")
            
        self.top_table.pack(fill=tk.X, expand=1)
        
        # Create table of envelope values
        # env = level,rate x8
        # Create StringVars for table
        self.rate_vars = []
        self.level_vars = []
        
        self.env_table = ttk.Frame(self,borderwidth=2,relief="ridge")
        ttk.Label(self.env_table,text=' ',borderwidth=2,
                 relief="ridge").grid(row=0,
                                      column=0,sticky=(tk.E + tk.W)) # empty upper left cell
        for i in range(1,9):
            h = ttk.Label(self.env_table,text=str(i),borderwidth=2,relief="ridge")
            h.config(font=font_bold)
            h.grid(row=0,column=i,sticky=(tk.E+tk.W))
            
        # row labels
        ttk.Label(self.env_table,text="Rate:",borderwidth=2,
                 relief="ridge").grid(row=1,column=0,sticky=(tk.E+tk.W))
        ttk.Label(self.env_table,text="Level:",borderwidth=2,
                 relief="ridge").grid(row=2,column=0,sticky=(tk.E+tk.W))


        for i in range(0,8):
            new_rate = tk.StringVar()
            new_level = tk.StringVar()
            self.rate_vars.append(new_rate)
            self.level_vars.append(new_level)
            ttk.Label(self.env_table,textvariable=new_rate,width=3,borderwidth=2,
                     relief="ridge").grid(row=1,column=i+1,sticky=(tk.E+tk.W)) # rate label
            ttk.Label(self.env_table,textvariable=new_level,width=3,borderwidth=2,
                     relief="ridge").grid(row=2,column=i+1,sticky=(tk.E+tk.W)) # level label

        self.env_table.grid_columnconfigure(0,weight=1)
        self.env_table.pack(fill=tk.X, expand=1)

        
    def refresh(self, num):
        """Refresh this view for a new tone specified by 'num'."""
        if self.mode == 'TVF':
            sus = app.get_tone_value('TVF ENV SUSTAIN POINT',num)
            end = app.get_tone_value('TVF ENV END POINT',num)
            env = app.get_tone_value('TVF ENV',num)
        else:
            sus = app.get_tone_value('TVA ENV SUSTAIN POINT',num)
            end = app.get_tone_value('TVA ENV END POINT',num)
            env = app.get_tone_value('TVA ENV',num)
        
 
        if env is None:
            # Envelope data is not available for this format, set all vars to None and return
            self.sustain.set(None)
            self.endpoint.set(None)
            for var in self.rate_vars:
                var.set(None)
            for var in self.level_vars:
                var.set(None)
            return

        self.env_table.update()
            
        # this is coded to TVF but the binding is the same
        self.sustain.set(str(formatting.format_value('TVF ENV SUSTAIN POINT',sus)))
        self.endpoint.set(str(formatting.format_value('TVF ENV END POINT',end)))
        
        # split env apart into separate rate and level lists
        rate = []; level = []
        for i in range(0,len(env),2):
            level.append(env[i])
            rate.append(env[i+1])
            
        for (index, value) in enumerate(rate):
            self.rate_vars[index].set(str(value))
        for (index, value) in enumerate(level):
            self.level_vars[index].set(str(value))
            
        
class DiskLabelFrame(ttk.LabelFrame):
    """
    LabelFrame class for displaying a disk label.
    """
    def __init__(self, parent, label_raw: bytes, *args, **kwargs):
        super().__init__(parent, text="Disk Label", *args, **kwargs)
        
        self.row_strings = [] # List of StringVars for each row
        
        # Label will always contain 5 rows
        for i in range(5):
            # Create labels and assign StringVars to them
            self.row_strings.append(tk.StringVar())
            ttk.Label(self, textvariable=self.row_strings[i]).grid(row=i,sticky=tk.W)
            
        self.refresh() # Load label if available.
            
    def refresh(self):
        """Refresh the disk label text."""
        if not app.disk_is_empty():
            label = formatting.readable_disk_label(app.get_disk_label())
        
            for i, line in enumerate(label.splitlines()):
                self.row_strings[i].set(line)
            
class ProgressWindow(tk.Toplevel):
    """Modal dialog to display progress, mainly for multiple sample export."""
    def __init__(self, text='Exporting samples...', *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.geometry("220x90")
        self.resizable(width=False,height=False)
        self.protocol("WM_DELETE_WINDOW", self._intercept_close)
        self.status_text = tk.StringVar()
        self.status_text.set(text)
        tk.Label(self,textvariable=self.status_text).pack()
        self.progress = ttk.Progressbar(self, orient = tk.HORIZONTAL, 
              length = 200, mode = 'determinate')
        self.progress.pack()
        
    def update_progress(self, *, steps=10):
        """
        Callback to update the progress bar.
        Steps is the number of steps the action is expected to take.
        """
        increment = 100 / steps # amount to increment by
        self.progress['value'] += increment
        self.update()
        
        if self.progress['value'] >= 100:
            self.status_text.set("Action complete.")
            tk.Button(self,text="Done",command=self.destroy).pack(pady=10)
            
    def _intercept_close(self):
        """Intercept window close function."""
        pass
